$(document).ready(function() {
    $('.js-data-table').DataTable({
        "language": {
            "lengthMenu": "_MENU_ recetas por página",
            "search": "Buscar",
            "zeroRecords": "La busqueda no dio ningún resultado.",
            "info": "Viendo página _PAGE_ de _PAGES_",
            "infoEmpty": "La base de datos de recetas no se ha podido cargar.",
            "infoFiltered": "(filtrado de entre _MAX_ recetas)",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "paginate": {
                "first":      "Primera",
                "last":       "Última",
                "next":       "Siguiente",
                "previous":   "Anterior"
            }
        },
        responsive: true,
        "dom": '<"well well-sm clearfix"fl>t<"dataTables_bottom"ip>'
    });

    if ($('.js-sticky').length) {
        var sticky = new Waypoint.Sticky({
            element: $('.js-sticky')[0]
        });
    }

    $.fn.markdown.messages['es'] = {
        'Bold': "Negrita",
        'Italic': "Itálica",
        'Heading': "Título",
        'URL/Link': "Insertar un vínculo",
        'Image': "Insertar una imagen",
        'List': "Lista",
        'Preview': "Previsualizar",
        'heading text': "ingrese el texto del título",
        'strong text': "ingrese el texto en negrita",
        'emphasized text': "ingrese el texto en itálica",
        'enter link description here': "ingrese el texto del vínculo",
        'Insert Hyperlink': "Ingrese la URL del vínculo",
        'enter image description here': "ingrese la descripción de la imagen",
        'Insert Image Hyperlink': "Ingrese la URL de la imagen",
        'enter image title here': "ingrese el título de la imagen",
        'list text here': "ingrese el texto de la lista"
    };

    $(".js-markdown").markdown({
        height: 400,
        autofocus:false,
        savable:false,
        language: 'es',
        hiddenButtons: ['cmdCode', 'cmdQuote', 'cmdImage']
    });

    var title = ['guide-title.jpg', 'La parte principal del nombre de la receta siempre está al comienzo y se muestra en una tipografía más grande.'];
    var title_ext = ['guide-title-ext.jpg', 'La parte extendida del título se muestra en una tipografía mas pequeña y continúa del nombre principal.'];
    var desc = ['guide-title-ext.jpg', 'La descripción de la receta es un texto breve que se muestra debajo del título extendido.'];
    var image = ['guide-image.jpg', 'La imagen principal se muestra tanto en la receta como en el listado de recetas de la región que corresponda.'];
    var portion = ['guide-portion.jpg', 'La cantidad o porciones que resultan de esta receta se muestran debajo del título "Ingredientes".'];
    var ingredients = ['guide-ingredients.jpg', 'El listado de ingredientes principales se muestra debajo de la cantidad o porciones.'];
    var ingredients_add = ['guide-ingredients-add.jpg', 'Los ingredientes adicionales son distintos grupos ubicados debajo de los principales, cada uno con su título descriptivo.'];
    var procedure = ['guide-procedure.jpg', 'El procedimiento de la receta se muestra debajo de la imagen y los ingredientes.'];
    var tips = ['guide-tips.jpg', 'El "tip" se muestra debajo del procedimiento.']; 
    var dato = ['guide-dato.jpg', 'El dato se muestra debajo del procedimiento. El título corresponde al texto que le sigue a "El dato:".'];

    $('*[data-guide]').click(function(){
        var data = $(this).data('guide');
        var dataImg = eval(data)[0];
        var dataText = eval(data)[1];

        $('.js-guide-image').show();
        $('.js-guide-image').attr('src', 'img/'+dataImg);
        $('.js-guide-caption').text(dataText);
    });

    var newIngredient = '<div class="row ingredient"><div class="col-xs-2"><input type="text" class="form-control"></div><div class="col-xs-9"><input type="text" class="form-control"></div><div class="col-xs-1"><button type="button" class="btn btn-danger js-delete-ingredient"><i class="glyphicon glyphicon-trash"></i></button></div></div>';

    var newIngredientGroup = '<div class="well well-sm js-ingredient-group"><input type="text" class="form-control" placeholder="Nombre del grupo"><hr><div class="js-ingredient-container"><div class="row ingredient"><div class="col-xs-2"><input type="text" class="form-control"></div><div class="col-xs-10"><input type="text" class="form-control"></div></div></div><div class="btn-group ingredient-actions"><button type="button" class="btn btn-default js-add-ingredient"><i class="glyphicon glyphicon-plus"></i> Agregar ingrediente</button><button type="button" class="btn btn-danger js-delete-ingredient-group"><i class="glyphicon glyphicon-trash"></i> Eliminar grupo</button></div></div>';

    $(document).on('click', '.js-delete-ingredient', function(){
        $(this).closest('.ingredient').remove();
    });

    $(document).on('click', '.js-add-ingredient', function(){
        $(this).parent().siblings('.js-ingredient-container').append(newIngredient);
    });

    $(document).on('click', '.js-add-ingredient-group', function(){
        $(this).siblings('.js-ingredient-group-container').append(newIngredientGroup);
    });

    $(document).on('click', '.js-delete-ingredient-group', function(){
        $(this).closest('.js-ingredient-group').remove();
    });

    $('.js-selectize').selectize();
});