$(document).ready(function(){
    // Viewport units fix

    window.viewportUnitsBuggyfill.init();


    // FastClick

    FastClick.attach(document.body);
    

    // Mobile navigation

    $('.js-toggle-menu').click(function(){
        if (!$('body').hasClass('mobile-menu-open')) {
            $('body').addClass('mobile-menu-open');
            $('.js-mobile-menu').show(0, function(){
                $('.js-mobile-menu-bg').stop().velocity({opacity: 1});
                $('.js-mobile-menu li').stop().velocity("transition.slideLeftIn", { duration: 800, stagger: 100 });
            });
        } else {
            $('body').removeClass('mobile-menu-open');
            $('.js-mobile-menu-bg, .js-mobile-menu li').stop().velocity({opacity: 0}, function(){
                $('.js-mobile-menu').hide(0);
            });
        }
    });


    // Dropdown

    $('.js-dropdown').on('mouseenter', function(){
        $(this).children('ul').addClass('is-visible');
    }).on('mouseleave', function(){
        $(this).children('ul').removeClass('is-visible');
    });

    $('.js-dropdown').on('click', function(){
        $(this).children('ul').show(0);
    });

    $(document).click(function(event) {
        if(!$(event.target).closest('.js-dropdown').length) {
            $('.js-dropdown').children('ul').hide(0);
        }
    });


    // Login/register/forgotten password screens

    $(document).on('click', '.js-forgot-pass', function(){
        $('#modal-main').removeClass('active');
        $('#modal-password').addClass('active');
    });

    $(document).on('click', '.js-register', function(){
        $('#modal-main').removeClass('active');
        $('#modal-register').addClass('active');
    });


    // Mock login
    $(document).on('click', '.js-login-btn', function(){
        $(this).children('span').text('').addClass('loading');

        setTimeout(function(){
            $('.js-login-title').text('Error: email o contraseña incorrectos.').addClass('has-error');
            $('.js-login-btn').children('span').text('Ingresar').removeClass('loading');
        }, 1000);
    });

    // Mock register
    $(document).on('click', '.js-register-btn', function(){
        $(this).children('span').text('').addClass('loading');

        setTimeout(function(){
            $('.js-register-error').fadeIn(500);
            $('.js-register-btn').children('span').text('Ingresar').removeClass('loading');
        }, 1000);
    });

    // Mock forgotten password
    $(document).on('click', '.js-password-btn', function(){
        $(this).children('span').text('').addClass('loading');

        setTimeout(function(){
            $('.js-password-form').fadeOut(500, function(){
                $('.js-password-success').fadeIn(500);
            });
        }, 1000);
    });


    // Home slider

    var onSliderInit = function(){
        $('.js-cover').cover();
        $('.hslider__slide').each(function(){
            $(this).imagesLoaded()
            .done(function(instance){
                var slide = $(instance)[0].elements[0];
                $(slide).addClass('img-loaded');
            });
        });
    };

    $(window).resize(function(){
        var vh = $(window).height();
        $('.js-hslider').height(vh-64);
    }).resize();

    $('.js-hslider > div').shuffle();

    $('.js-hslider').slick({
        infinite: true,
        autoplay: true,
        autoplaySpeed: 4500,
        dots: true,
        appendArrows: false,
        speed: 750,
        cssEase: 'ease-in-out',
        fade: true,
        init: onSliderInit()
    });

    // Recipe

    $(window).resize(function(){
        var vh = $(window).height();
        $('.js-recipe-top').height(vh-64);

        var inner = $('.js-ingr-width').innerWidth();
        $('.js-ingr-shadow').width(inner);
    }).resize();

    $('body').waypoint(function(direction) {
        $('.js-ingr-btn').toggleClass('is-showing');
    }, {offset: -100});

    $('.js-footer').waypoint(function(direction) {
        $('.js-ingr-btn').toggleClass('is-showing');
    }, {offset: '90%'});

    $('.js-ingr-btn').click(function(){
        if ($('body').hasClass('viewing-ingredients')) {
            $('body').removeClass('viewing-ingredients');
            $('.js-ingr-btn span').text('Ver ingredientes');
        } else {
            $('body').addClass('viewing-ingredients');
            $('.js-ingr-btn span').text('Ocultar ingredientes');
        }
    });

    $('.recipe__main__bg').each(function(){
        $(this).imagesLoaded()
        .done(function(instance){
            var elem = $(instance)[0].elements[0];
            $(elem).addClass('img-loaded');
        });
    });

    // Recipe favs

    $('.js-fav').click(function() {
        if ($(this).hasClass('is-fav')) {
            $('.js-fav').removeClass('is-fav').children('span').text('Agregar a favoritos');
        } else {
            $('.js-fav').toggleClass('is-fav').children('span').text('Borrar de favoritos');
        }
    });


    // Recipe social (deshabilitado por el momento)

    /*$('.js-recipe-social').waypoint(function(direction) {
        $('.js-recipe-social').toggleClass('is-stuck');

        var left = $('.js-procedure').offset().left;

        if (direction === 'down') {
            $('.js-recipe-social').css({left: left-59});
        } else {
            $('.js-recipe-social').css({left: 0});
        }
    }, {offset: 64});*/

    
    // Region

    $('.js-region-text').readmore({
        speed: 500,
        collapsedHeight: 100,
        heightMargin: 30,
        moreLink: '<button class="region__top__more">Más sobre esta región</button>',
        lessLink: '',
        afterToggle: function(){
            $('.js-region-top').addClass('is-expanded');
        }
    });

    var $grid = $('.js-region-grid').isotope({
        itemSelector: '.region__grid__item',
        percentPosition: true,
        masonry: {
            columnWidth: '.region__grid__sizer'
        }
    });

    $grid.imagesLoaded().progress(function() {
        $grid.isotope('layout');
    });

    $('.js-region-filters').on('click', 'button', function() {
        $('.js-region-filters button').removeClass('active');
        $(this).addClass('active');
        var filterValue = $(this).attr('data-filter');
        $grid.isotope({ filter: filterValue });
    });

    $('.js-region-filters button').on('click', function() {
        if ($(window).width() > 480) {
            $.smoothScroll({
                scrollTarget: '.js-region-filters',
                offset: -64
            });
        } else {
            $.smoothScroll({
                scrollTarget: '.js-region-grid',
                offset: -83
            });
        }
    });


    // Lady load images

    $('.js-lazy').lazyload({
        effect: 'fadeIn',
        threshold: 2000
    });


    // Smooth scroll

    $('.js-scroll').smoothScroll({
        offset: -64
    });


    // Region image galleries
    
    $('.js-open-gallery').click(function(){
        // Pics list
        var region = $(this).attr('data-region');

        var items = [];
        
        switch (region) {
            case 'bsas':
                items = [
                    {
                        src: 'img/region/bsas.jpg',
                        w: 1500,
                        h: 1125
                    },
                    {
                        src: 'img/region-galleries/bsas/1.jpg',
                        w: 1500,
                        h: 843
                    },
                    {
                        src: 'img/region-galleries/bsas/2.jpg',
                        w: 945,
                        h: 945
                    },
                    {
                        src: 'img/region-galleries/bsas/3.jpg',
                        w: 1024,
                        h: 639
                    },
                    {
                        src: 'img/region-galleries/bsas/4.jpg',
                        w: 1500,
                        h: 996
                    },
                    {
                        src: 'img/region-galleries/bsas/5.jpg',
                        w: 1500,
                        h: 1000
                    },
                    {
                        src: 'img/region-galleries/bsas/6.jpg',
                        w: 1181,
                        h: 804
                    },
                    {
                        src: 'img/region-galleries/bsas/7.jpg',
                        w: 1500,
                        h: 1060
                    }
                ];
                break;
            case 'caba':
                items = [
                    {
                        src: 'img/region/caba.jpg',
                        w: 1500,
                        h: 1060
                    },
                    {
                        src: 'img/region-galleries/caba/1.jpg',
                        w: 1500,
                        h: 1000
                    },
                    {
                        src: 'img/region-galleries/caba/2.jpg',
                        w: 1500,
                        h: 997
                    },
                    {
                        src: 'img/region-galleries/caba/3.jpg',
                        w: 996,
                        h: 1500
                    },
                    {
                        src: 'img/region-galleries/caba/4.jpg',
                        w: 1500,
                        h: 1000
                    },
                    {
                        src: 'img/region-galleries/caba/5.jpg',
                        w: 1500,
                        h: 1060
                    },
                    {
                        src: 'img/region-galleries/caba/6.jpg',
                        w: 1500,
                        h: 1122
                    }
                ];
                break;
            case 'centro':
                items = [
                    {
                        src: 'img/region/centro.jpg',
                        w: 1500,
                        h: 1060
                    },
                    {
                        src: 'img/region-galleries/centro/1.jpg',
                        w: 1500,
                        h: 997
                    },
                    {
                        src: 'img/region-galleries/centro/2.jpg',
                        w: 1500,
                        h: 1041
                    },
                    {
                        src: 'img/region-galleries/centro/3.jpg',
                        w: 800,
                        h: 471
                    },
                    {
                        src: 'img/region-galleries/centro/4.jpg',
                        w: 1500,
                        h: 843
                    },
                    {
                        src: 'img/region-galleries/centro/5.jpg',
                        w: 1500,
                        h: 843
                    },
                    {
                        src: 'img/region-galleries/centro/6.jpg',
                        w: 939,
                        h: 600
                    },
                    {
                        src: 'img/region-galleries/centro/7.jpg',
                        w: 1500,
                        h: 1000
                    }
                ];
                break;
            case 'cuyo':
                items = [
                    {
                        src: 'img/region/cuyo.jpg',
                        w: 1500,
                        h: 1000
                    },
                    {
                        src: 'img/region-galleries/cuyo/1.jpg',
                        w: 1500,
                        h: 1000
                    },
                    {
                        src: 'img/region-galleries/cuyo/2.jpg',
                        w: 1500,
                        h: 996
                    },
                    {
                        src: 'img/region-galleries/cuyo/3.jpg',
                        w: 1500,
                        h: 1000
                    },
                    {
                        src: 'img/region-galleries/cuyo/4.jpg',
                        w: 1500,
                        h: 843
                    },
                    {
                        src: 'img/region-galleries/cuyo/5.jpg',
                        w: 1051,
                        h: 700
                    },
                    {
                        src: 'img/region-galleries/cuyo/6.jpg',
                        w: 1408,
                        h: 944
                    }
                ];
                break;
            case 'nea':
                items = [
                    {
                        src: 'img/region/nea.jpg',
                        w: 1500,
                        h: 843
                    },
                    {
                        src: 'img/region-galleries/nea/1.jpg',
                        w: 999,
                        h: 1500
                    },
                    {
                        src: 'img/region-galleries/nea/2.jpg',
                        w: 1500,
                        h: 1231
                    },
                    {
                        src: 'img/region-galleries/nea/3.jpg',
                        w: 1500,
                        h: 1017
                    },
                    {
                        src: 'img/region-galleries/nea/4.jpg',
                        w: 1031,
                        h: 1500
                    },
                    {
                        src: 'img/region-galleries/nea/5.jpg',
                        w: 1500,
                        h: 1060
                    },
                    {
                        src: 'img/region-galleries/nea/6.jpg',
                        w: 1500,
                        h: 1060
                    }
                ];
                break;
            case 'noroeste':
                items = [
                    {
                        src: 'img/region/noroeste.jpg',
                        w: 1500,
                        h: 1060
                    },
                    {
                        src: 'img/region-galleries/noroeste/1.jpg',
                        w: 1500,
                        h: 1000
                    },
                    {
                        src: 'img/region-galleries/noroeste/2.jpg',
                        w: 1500,
                        h: 1000
                    },
                    {
                        src: 'img/region-galleries/noroeste/3.jpg',
                        w: 1500,
                        h: 1000
                    },
                    {
                        src: 'img/region-galleries/noroeste/4.jpg',
                        w: 1500,
                        h: 1060
                    },
                    {
                        src: 'img/region-galleries/noroeste/5.jpg',
                        w: 1500,
                        h: 1060
                    },
                    {
                        src: 'img/region-galleries/noroeste/6.jpg',
                        w: 1500,
                        h: 997
                    }
                ];
                break;
            case 'patagonia':
                items = [
                    {
                        src: 'img/region/patagonia.jpg',
                        w: 1500,
                        h: 985
                    },
                    {
                        src: 'img/region-galleries/patagonia/1.jpg',
                        w: 1500,
                        h: 996
                    },
                    {
                        src: 'img/region-galleries/patagonia/2.jpg',
                        w: 1500,
                        h: 1122
                    },
                    {
                        src: 'img/region-galleries/patagonia/3.jpg',
                        w: 1500,
                        h: 1000
                    },
                    {
                        src: 'img/region-galleries/patagonia/4.jpg',
                        w: 1500,
                        h: 1060
                    },
                    {
                        src: 'img/region-galleries/patagonia/5.jpg',
                        w: 993,
                        h: 1050
                    },
                    {
                        src: 'img/region-galleries/patagonia/6.jpg',
                        w: 1500,
                        h: 1060
                    }
                ];
                break;
        }

        if (items.length) {
            var gallery = new PhotoSwipe($('.js-region-gallery')[0], PhotoSwipeUI_Default, items); 
            gallery.init();
        } else {
            console.log('No photos loaded for gallery.');
        }
    });
});